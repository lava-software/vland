.. _what_is_vland:

What is VLANd?
##############

VLANd is a python program intended to make it easy to manage
port-based VLAN setups across multiple switches in a network. It is
designed to be vendor-agnostic, with a clean pluggable driver API to
allow for a wide range of different switches to be controlled
together.

Various switch vendors have management applications available which
should be able to do a similar job, but they're typically not
scriptable, or locked to only control their own vendor's
equipment. VLANd is designed (and hoped!) to be better. We've found
that other network management programs also exist, but either they
don't manage VLANs in the way we want or they depend on large
frameworks like Openstack.

VLANd currently includes drivers for different models of switch from
four different vendors:

* ``CiscoCatalyst``  for the Cisco Catalyst 3750 (and compatible)
* ``CiscoSX300``     for the Cisco SF300 and SG300 (and compatible)
* ``NetgearXSM``     for the Netgear XSM 7224S (and compatible)
* ``TPLinkTLSG2XXX`` for the TP-Link TL-SG2216 (and compatible - also
  tested on TL-SG2424 and T1700G-28TQRev2)
* ``Mellanox``       for the Mellanox SN2100 (and compatible)

These cover a range of switches across a huge range of prices, from
100M up to 100G ports. VLANd supports inter-operable VLANs across all
these devices, verified using the Linaro test lab and our test suite.

VLANd also includes a ``Dummy`` switch driver which can be used for
testing and validation. Read the comments in ``drivers/Dummy.py`` for
more details on how to use it.

It is expected that more drivers will be included in future
releases. Please feel free to propose more target devices, or (better)
send patches!

.. _why_vland:

Why VLANd?
==========

VLANd has been written primarily to accompany Linaro's LAVA
platform. LAVA is the Linaro Automated Validation Architecture - see
https://www.lavasoftware.org/ for more information.

LAVA started off as a framework to help perform validation and testing
on simple single devices such as Android development boards. Since
then, the range of tests and devices has grown hugely. Some users have
added multi-node tests, i.e. tests that start up multiple devices that
run tests against each other (e.g. client-server testing). The next
step on from that is to test devices with multiple network interfaces,
where on top of the client-serer testing it is required that their
connectivity can be controlled and isolated (e.g. for network
performance testing).

This can be done by simply changing physical network connections, of
course, but that doesn't scale for sequences of tests and it makes a
lie of the "Automated" bit in the LAVA definition.

VLANd is the solution that we have implemented for this problem. It
provides a simple VLAN management abstraction for users. LAVA can use
VLANd to set up complex networks on demand as tests are initiated, and
can then tear them down again when tests are complete.

.. _dependencies:

Dependencies
============

VLANd uses the ``postgres`` database server and ``psycopg2`` for the
back-end database. See ``db/setup_db.py`` for help in setting up the
database for access.

If you want to use the visualisation feature, you'll also need to
install ``python-pil`` (used for generating the graphics) and a font
for it to use. The default font needed is ``Inconsolata``, but others
should work too.

.. _how_to_use:

How to use
==========

Things are most easily run as the ``vland`` user, with appropriate
permissions in the database for that user.

The core VLANd code is simple to configure - see :ref:`vland_cfg` for
an example config file. Once your copy of VLANd is running, use the
administration utility :ref:`vland-admin` to control what it does. For
now, this is single-threaded and has *no* security. This is planned to
change in the future, but for early development this has not been a
priority.

The visualisation interface has very few configuration options as
yet - again, see the default vland.cfg.

VLANd can be run directly from a source checkout, or it is also
available packaged for Debian.

.. _future_plans:

Potential future plans
======================

In no particular order:

* More documentation
* More drivers to control more devices, both real and virtual
* Switch to python3 (needed to move away from python-gd first, now done)
* Improved visualisation options
* Switch to using sqlite instead of postgres for the simple state database
* Better (some!) security
* Background sanity checking
* Other filtering options - MAC-based rather than just port-based
  (needed to support virtual machines, etc.)

If you have other ideas, please talk to us!

.. _contact:

Contact
=======

VLANd mailing list: vland-devel@linaro.org

.. _code:

Code and issue tracker
======================

VLANd is maintained using the LAVA GitLab. See
https://git.lavasoftware.org/lava/vland

License
=======

VLANd is Copyright 2014-2019 Linaro Limited, and is distributed under
the GNU General Public License, version 2 (or any later version). See
the accompanying COPYING file for more details.
