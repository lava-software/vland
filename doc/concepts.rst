 .. index:: concepts

.. _vland_concepts:

VLANd concepts
##############

VLANd is designed to manage a set of switches and allow for simple
cross-vendor VLAN control. To accomplish this, it models 4 different
concepts:

.. index:: port

.. _concepts_port:

Port
====

VLANd is fundamentally designed to control VLANs using port-based
mappings, so the **port** is the most important concept for VLANd. All
switches, trunks and VLANs are defined by the ports that they
use/contain. All of the core APIs for using VLANd to change the setup
of a network take a port ID as an argument.

Ports have multiple attributes that are tracked by the system:

* the port ID (a unique number to identify the port within the
  system). All port-related API commands will need the port ID as a
  reference.
* the name of the port (as identified by the switch software - see
  :ref:`port numbering <port_terminology>`)
* the number of the port (the simple numeric index of the port as
  listed by the switch software - see :ref:`port numbering
  <port_terminology>`)
* the switch ID (to track which switch a port belongs to)
* two state flags:

  * is the port **locked**? (see :ref:`lock_ports` for more details)
  * is the port in **trunk** mode? (i.e. should it see all traffic for
    all VLANs? If not, it is described as **access** mode. See
    :ref:`switch_port_modes` for more details.)

* two VLAN IDs:

  * the **current** VLAN ID (the VLAN that the port is currently part
    of, if the port is in **access** mode)
  * the **base** VLAN ID (an alternative VLAN that the port can easily
    be reverted to, if the port is in **access** mode. See
    :ref:`base_vlans` for more details.)

* the trunk ID - if the port is part of a trunk, track which
  trunk. Used only for the :ref:`visualisation interface
  <visualisation_interface>` so far.

.. index:: locking ports

.. _lock_ports:

Locking ports
-------------

As a safety feature when working with VLANd, ports can be marked as
**locked** by the admin. Once a port is locked, VLANd will refuse to
apply any other changes to that port (e.g. changing the current VLAN,
os switching the port mode) until it is unlocked. It's recommended to
use this feature to stop accidentally changing the configuration of
ports where infrastructure devices (e.g. UPS, the server running
VLANd) are connected.

.. index:: port modes, switchport modes, access mode, trunk mode

.. _switch_port_modes:

Port modes
----------

Different vendors and different switches have different ways of
describing how ports are configured when working with VLANs. VLANd
simplifies the possible configuration to allow interoperability
between different devices. Some devices are much more capable and can
do much more advanced things with powerful control of packet flow, but
VLANd does not support this.

The simple terms chosen here will **not** necessarily match up with
the documentation for any given switch, for similar reasons.

* A port in **access mode** is configured to work with **one specific
  VLAN** only. All traffic from a device connected to that port
  (sometimes described as ``ingress``) will be tagged with that
  VLAN. All traffic leaving via the same port will have the VLAN tag
  removed. Traffic to/from any other VLANs will be filtered.

  **All normal devices should be connected to access ports.**

* A port in **trunk mode** is configured to work with **all**
  VLANs. The switch will not make any modifications to the packets
  passing through this port, either ingress or egress. **All** traffic
  for all VLANs will pass through this port. Connections between
  switches therefore need to be in trunk mode to allow for traffic on
  all VLANs to propagate to all switches.

.. warning:: If you are using switches with big differences in speed,
	     be careful - the trunk ports on the slowest switches will
	     become a bottleneck.

.. index:: switch

.. _concepts_switch:

Switch
======

To VLANd, a **switch** is simple thing - it's a container for a number
of **ports**.

There are only two switch attributes that VLANd tracks in the
database:

* the switch ID (a unique number to identify the switch within the
  system). All switch-related API commands will need the switch ID as
  a reference.
* the name of the switch (matching the name specified in
  :ref:`vland.cfg <vland_cfg>`)

Each switch must be named uniquely, and some details must be entered
in the configuration file (:ref:`vland.cfg <vland_cfg>`) - namely the
driver to use for the switch and the username/password details to use
to access it.

.. index:: vlan

.. _concepts_vlan:

VLAN
====

VLANs are defined across switches using the ``802.1q`` standard, and
also tracked in the VLANd database. There are several attributes that
VLANd cares about:

* the VLAN ID (a unique number to identify the VLAN within the
  system). All VLAN-related API commands will need the VLAN ID as a
  reference.
* the name of the VLAN (as specified by the user at creation time,
  following the 802.1q standard for length/allowed characters etc.)
* the VLAN tag (integer, in the range 1-4096 as per the 802.1q
  standard)
* the creation time for the VLAN (used for diagnostics)
* is the VLAN a :ref:`base VLAN <base_vlans>`?

.. index:: base VLANs, isolation VLANs

.. _base_vlans:

Base VLANs
----------

Some multi-homed devices may cause trouble if all of their network
interfaces are visible on the same VLAN. This can happen a few
ways. Some devices share the same MAC address on multiple
interfaces. On others, the firmware may automatically attempt DHCP
configuration on all interfaces and using the "wrong" interface will
not work reliably. In many cases, a test job will want to use one
specific interface as the **primary** - booting and mounting of an NFS
root will want to be done on that interface, while others are set
aside for network tests.

In these situations, it is desirable to keep **non-primary**
interfaces isolated away from the :ref:`default_vlan`. To support
this, VLANd tracks a **base** VLAN for each port. When a test job is
started, VLANs can be created and ports can be assigned to them as
wanted in that test. At the end of the test, each of the ports can be
safely moved back to their base VLAN without having to know its
details.

Obviously, the base VLANs will need to be configured in the system for
this to be useful!

.. index:: default VLAN

.. _default_vlan:

Default VLAN
------------

The default VLAN for most switches will normally be tagged as **1**,
and this is typically sensible for the vast majority of uses. In the
unlikely case that you need to choose a different default VLAN, it can
be configured in the :ref:`vland_cfg` file.

.. index:: trunk

.. _concepts_trunk:

Trunk
=====

Trunks are connections between switches. These are tracked in VLANd
very simply, as point-to-point links. This information is **only**
used to help describe topology for the :ref:`visualisation interface
<visualisation_interface>` thus far.

Ports in :ref:`trunk mode <switch_port_modes>` control how the network
behaves, but VLANd does not care about specific trunks for that.

There are only two trunk attributes that VLANd tracks in the database:

* the trunk ID (a unique number to identify the trunk within the
  system). All trunk-related API commands will need the trunk ID as a
  reference.
* the creation time for the trunk (to be used for diagnostics)
