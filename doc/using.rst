 .. index:: using

.. _using_vland:

Using VLANd
###########

 .. index:: setup

.. _setup:

Initial setup
=============

To set up a VLANd instance, you'll need to install the software. then
configure the initial database access - see ``db/setup_db.py`` for
help with that.

Next, tell VLANd about all the switches that you want to use, in
:ref:`vland.cfg <vland_cfg>`. Each switch must have a unique ``name``
defined (or an IP address if you don't have DNS set up). VLANd also
needs details of which driver to use and how to log in.

Populate the database to match your switch configuration. That's
possible by directly adding all the switches, ports and VLANs
individually using the :ref:`vland-admin` tool manually, but it will
be very long-winded and error-prone. Instead, :ref:`vland-admin` has a
subcommand ``auto_import_switch`` which will do most of the hard work
for you - run it once per switch. For sanity, check the configuration
of your switches before you start. For example, if you have VLANs
already defined, make sure they're already consistent (i.e. matching
tags and names). ``auto_import_switch`` will try to do the right thing
for you based on what it reads from your switches, but it will fail if
it can't understand your setup.

Once you have all the ports, switches and VLANs imported or set up in
the database, configure the ports appropriately:

* any inter-switch connections should be set into :ref:`trunk mode
  <switch_port_modes>` so that they will pass traffic for all
  VLANs. (``vland-admin set_port_mode``)

* (optionally) if you plan on using the
  :ref:`visualisation_interface`, add trunks showing your inter-switch
  connections so that it can show them. (``vland-admin create_trunk``)

* all other ports should be set into :ref:`access mode
  <switch_port_modes>`. (``vland-admin set_port_mode``)

* if you have infrastructure devices connected to your switches, it is
  recommended that you :ref:`lock <lock_ports>` them to stop users
  accidentally changing configuration and breaking your
  infrastructure. (``vland-admin lock_port``)

* if you have any multi-homed devices attached to your network, you
  may want to :ref:`isolate <base_vlans>` some of their connected
  ports. (``vland-admin create_vlan``, ``vland-admin
  set_port_base_vlan``, ``vland-admin restore_port_to_base_vlan``)

 .. index:: vland-admin

.. _vland-admin:

vland-admin
===========

``vland-admin`` is a tool designed to expose the VLANd APIs to local
admins. It's basically a thin wrapper around the APIs. Using it, you
can look up lots of details about the system and also make
changes. Here's a summary of the available commands; run ``vland-admin
command --help`` for more details on each one.

.. literalinclude:: vland-admin-commands.txt

.. index:: vland-admin tips, command line tips

.. _vland-admin-tips:

vland-admin tips
----------------

Start by listing the various items (e.g. ``vland-admin
list_all_ports``) if you're not sure what you're after.

``vland-admin`` will always want to use IDs for its APIs (to reference
switches, ports, VLANs or trunks. Remember to use the lookup APIs
(e.g. ``vland-admin lookup_port_by_switch_and_number``) to work out
the ID that you need before attempting to make changes.

If you're looking to work on specific ports manually and you're not
sure of the IDs, use the :ref:`visualisation_interface`. Simply run
your mouse over the various ports on the switch you're looking at, and
it will show you more details in the popup. This will likely be much
quicker than calling the lookup APIs on the command line over and
over.

 .. index:: vland api, using the api

.. _api_usage:

How to use the VLANd API
========================

There isn't (yet!) a friendly module to expose the API. The easiest
way to use things at the moment is to copy what ``vland-admin`` does::

    from Vland.ipc.ipc import VlanIpc
    ipc = VlanIpc()
    ipc.client_connect(*server*, *port*)
    msg['client_name'] = 'my-application'
    msg['type'] = *type*
    ipc.client_send(msg)
    ret = ipc.client_recv_and_close()
    *decode msg*

 .. index:: common API tasks

.. _common_api_tasks:

Common tasks with the API
=========================

VLANd was designed to work with LAVA, providing API access to VLANs as
LAVA tests need them. LAVA's typical setup in a VLANd-enabled test
will have several test devices included, each with multiple network
interfaces. The test writer will be asking for one or more VLANs to be
set up.

As the test devices are configured in a typical lab, LAVA will have a
switch name and a port number for each of the network interfaces. The
sequence of calls in a simple test will look something like:

* connect to VLANd via IPC
* for each network interface used:

  * look up the switch ID from the switch name
  * lookup the port ID from the switch ID and the port number

* create the specified VLANs
* move ports to the VLANs as desired
* *run the test*
* return each port to its base VLAN
* delete each of the VLANs that was created
* disconnect from VLANd

The code to do this in LAVA is inside the LAVA dispatcher - see
https://git.lavasoftware.org/lava/lava/blob/master/lava_dispatcher/protocols/vland.py
if you'd like to see exactly how it's done.

 .. index:: troubleshooting

.. _troubleshooting:

Troubleshooting
===============

It is hoped that VLANd will work reliably and do just what you need!
But, like all software, it's possible that you might encounter
problems. Here are the things to try if VLANd appears to be
misbehaving:

* Check that VLANd is running.

* Check that the postgres database server is still running. If you
  have restarted it, you will also need to restart VLANd.

* Look in the VLANd logfile (``/var/log/vland.log`` by default) for
  errors. If there is a back-trace at the end, you've found a bug.

* Check the timestamps in the logfile to see how recent they are.

* Run a simple ``vland-admin status`` and see if you get a
  response. Errors from ``vland-admin`` here will typically suggest
  that VLANd is not running, or something else is hogging the comms
  socket.

* If API calls hang, maybe VLANd is stuck doing something else. When
  the command is received, it should show in the log . If not, then
  VLANd is definitely stuck somewhere. Try killing the VLANd
  process(es) and restarting, and see if things respond now.

The most common causes of VLANd problems are a missing database
connection (as mentioned earlier), or a misbehaving switch. Simply
restarting VLANd is normally enough to get things back up and running,
but depending on the state of the switches you might need to unwind
changes (VLAN creation, port moving, etc.) to put things back to
normal.

