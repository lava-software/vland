.. _port_numbering_naming:

Port numbering and naming in VLANd
##################################

This is a far more complex subject than it should be, and it may cause
problems for end users depending on their usage models. This is
particularly likely for those users with Cisco Catalyst 3750 switches
configured with extra internal modules for extra copper/fibre ports.

.. index:: port naming, port terminology, port name, port number

.. _port_terminology:

Terminology
===========

For consistency, throughout this document and within VLANd two terms
are used to identify switch ports:

* a **port name** is the (typically) alphanumeric identifier for a switch
  port *in software*, i.e. when using the CLI to manage a
  switch. Common naming styles vary significantly here; examples from
  the VLANd test rack in Linaro's lab include "Gi1/0/6", "Gi1/1/4",
  "Te1/1/2", "fa24", "gi3", "1/0/23"

* a **port number** is the (typically) numeric identifier for a switch
  port that is labelled visibly on the switch somewhere, typically
  next to the port or a group of ports. Not all switches limit
  themselves to numeric labels, though. The same examples from our
  test rack are described as "6", "G4/Te2", "24", "G3", "23G", "23T"

These examples include some of the problems - how do we try to match
these up!

.. _port_naming_problems:

The problems we've seen
=======================

On **all** of the switch models that we currently support, there is a
disconnect between the port names and the associated port numbers. The
port names are **not** directly shown to the user on any of our
switches, which is not a good start!

To a human operator, it is likely not very difficult to perform the
mapping from one scheme to the other in most cases. However, multiple
cases exist where it is difficult to map reliably from one identifier
to the other in software without needing extensive manual
configuration. For our VLAN control to be useful, we need to be able
to identify ports in a recognisable way for our users who have
connected devices to their network.

In the examples above, I showed 6 identifiers in each list but they
only correspond to 5 physical ports on the switches!

Problem 1: Disjoint port numbers
--------------------------------

On our Cisco SF300, there are two sets of ports: 48 10/100M RJ45 ports
and a further four 1G RJ45 ports. They're labelled externally as ports
1-48 and G1-G4; internally the names are fa1-fa48 and
gi1-gi4. Thankfully, this particular case isn't too hard to deal
with. The SG300 we have (which works with the same driver) simply has
52 ports labelled 1-52 and named gi1-gi52. The two switches look very
similar to the user.

Problem 2: Single physical ports with multiple possible port names
------------------------------------------------------------------

On our Cisco Catalyst 3750-X 24P, most of this is easy. It has 24 1G
ports, labelled 1 to 24 on the front panel. The port name "Gi/1/0/6"
corresponds to "6". However, we also have an extra SFP module fitted
that exposes 4 extra 1G interfaces (labelled G1 to G4). However,
there's more complication yet: G2 and G4 are **dual-personality**
ports and can also work as 10G ports when appropriate SFP+ modules are
inserted. These 1G and 10G ports are named differently in software
depending on which speed is in use:

+-------------+----------------------+
| Port number | Port name            |
+=============+======================+
| G1          | Gi1/1/1              |
+-------------+----------------------+
| G2/Te1      | Gi1/1/2 OR Te1/1/1   |
+-------------+----------------------+
| G3	      | Gi1/1/3              |
+-------------+----------------------+
| G4/Te2      | Gi1/1/4 OR Te1/1/2   |
+-------------+----------------------+

Our Catalyst 3750 also has an extra 10/100M port on the back as a
management port; VLANd explicitly ignores this port. Various other
models in the 3750 range can come in a whole range of different
configurations with 10/100M, 1G and 10G ports and it's very difficult
to identify naming and numbering schemes for all of these without
physical access to all of them.

Problem 3: Multiple physical ports which map to the same port name
------------------------------------------------------------------

Our Netgear M7300-24XF (aka XSM7224S) has 24 10G SFP+ ports and a
further 4 10G RJ45 ports. Those 4 copper ports replace the last 4 of
the SFP+ ports in a "combo" arrengement - if an SFP module is
inserted, the RJ45 port is disabled. Thus, on this switch we have the
following port numbers and port names:

+-------------+----------------------+
| Port number | Port name            |
+=============+======================+
| 1           | 1/0/1                |
+-------------+----------------------+
| ...         |                      |
+-------------+----------------------+
| 20          | 1/0/20               |
+-------------+----------------------+
| 21F OR 21T  | 1/0/21               |
+-------------+----------------------+
| ...         |                      |
+-------------+----------------------+
| 24F OR 24T  | 1/0/24               |
+-------------+----------------------+

This combo approach is also used on the TP-Link TL-SG2216; it has 16
1G RJ45 ports and 2 1G SFP ports which over-ride them:

+-------------+----------------------+
| Port number | Port name            |
+=============+======================+
| 1           | Gi1/0/1              |
+-------------+----------------------+
| ...         |                      |
+-------------+----------------------+
| 14          | Gi1/0/14             |
+-------------+----------------------+
| 15 OR 15F   | Gi1/0/15             |
+-------------+----------------------+
| 16 OR 16F   | Gi1/0/16             |
+-------------+----------------------+

These show the opposite problem to the Catalyst 3750 - if a user
connects to port 16 on the TP-Link, that at least will show up as one
consistent name in the CLI. However, should we be looking for port
number 16 or 16F when talking to the user?

.. index:: port naming

.. _port_naming_solution:

A (partial) solution in VLANd 0.4 onwards
=========================================

Hopefully the above descriptions will make it clear that this is a
hard problem to solve 100% reliably. However, it's a problem that must
at least partially be solved as end-users supplying port numbers
cannot be expected to know the port names that are used internally by
the switches but not exposed externally. Yet port names must be used
internally for all the interactions with the switches directly - this
is the only way that the software can work.

So, **port numbers** have been added as an extra column in the port
database. New APIs and methods have been added to do lookups by port
number as alternatives to lookups by port name, to make things easier
for end users. When adding ports using the admin interface, a port
number is now required too.

The backend of the auto_import_switch admin command (the recommended
way to add switches) will attempt to automatically allocate sensible
port numbers as it probes switches and finds new ports. The algorithm
chosen here for assigning port numbers is the simplest one that might
work for most of the switches and configurations that we
have. *Numeric* port numbers (i.e. without any prefix or suffix
letters) will be assigned to port names simply in the order that the
port names are listed via a switch's CLI, starting with "1". For
almost all our supported switches, this should make sense for a user
when trying to supply port numbers. However, *BE AWARE* of the
possibility that your port numbers may not always match the port
numbers written on your switch in some cases (e.g. the Cisco Catalyst
3750 as described above).

If this port number to port name mapping algorithm proves not to be
sufficient, future releases of VLANd may include options in the config
file to provide exact mappings. We'd prefer not to have to do this due
to the admin overhead it will impose (and the attendant possibility of
errors).
