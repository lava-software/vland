vland 0.8

 * Fix up daft errors missed in the code layout re-organisation. Make sure
   we import things from the python path properly.

 -- Steve McIntyre <steve.mcintyre@linaro.org>  Tue, 11 Dec 2018 17:20:39 +0000

vland 0.7

 * Added new driver to support the Mellanox SN2100 switch, using MLNX-OS
 * Renamed the admin program from "admin.py" to "vland-admin" to make
   packaging easier
 * Minor changes to the database code:
   + Automatic schema updates at vland startup
   + Add extra lock_reason field in the port table, and code to match
   + Use a different cursor type for row-based lookups to improve
     maintainability
 * Added new feaure - support for admins to store a reason when locking
   ports
 * Small changes in the web interface port popup:
   + Will now display the lock reason
   + Will now display the port name as displayed by the switch
 * Various small cleanups and bugfixes
 * Add python packaging setup, and tweak code layout to make it sensible
 * Add Debian packaging
 * Tweak text labels in the web interface
 * Improve error handling in the database startup code

 -- Steve McIntyre <steve.mcintyre@linaro.org>  Tue, 04 Dec 2018 15:15:39 +0000

vland 0.6

 * Major changes to admin interface command-line:
   + Switch from optparse to argparse for much better support of complex
     command lines, using sub-commands instead of arguments and named
     arguments instead of positional arguments. Example:
       $ admin.py --create_vlan test30 30 false
     now becomes
       $ admin.py create_vlan --name test30 --tag 30 --is_base_vlan false
    + admin.py will now exit non-zero when commands fail rather than
      (falsely) always indicate success.
 * Errors are tracked better internally, and distinguished in various
   places between "object not found" and "misc error".
 * Misc small bugfixes

 -- Steve McIntyre <steve.mcintyre@linaro.org>  Thu, 24 Mar 2016 17:19:39 +0000

vland 0.5

 * Large numbers of bugfixes and code cleanups
   + More robust error handling and logging
 * Lots of improvements to the visualisation code:
   + Bind to any interface, not just localhost so that the
     visualisation can be seen from other machines
   + Add an option to auto-refresh in the browser
   + Improve error logging
   + Add caching of graphics for improved performance
   + Improve compliance with HTTP headers
   + Add popup tooltips for more information about configuration of ports
 * Add a special dummy switch driver to help with debugging and testing
 * In database multi-element calls, sort the output for consistency
 * Add a database last-modified-time to help with caching
 * Make 404 errors more descriptive

 -- Steve McIntyre <steve.mcintyre@linaro.org>  Thu, 24 Dec 2015 12:16:29 +0000

vland 0.4

 * Large numbers of bugfixes and code cleanups
 * Code changes for integration with LAVA:
   + Added db.find_lowest_unused_vlan_tag()
   + create_vlan() with a tag of -1 will find and allocate the first
     unused tag automatically
 * Add port *numbers* as well as names to the ports database, to give
   human-recognisable references. See README.port-numbering for more
   details.
 * Add tracking of trunks, the inter-switch connections, needed for
   visualisation diagrams.
 * Add a simple http-based visualisation feature:
   + Generate network diagrams on-demand based on the information in the
     VLANd database, colour-coded to show port configuration
   + Generate a simple website to reference those diagrams.
 * Allow more ports to be seen on Catalyst switches
 * Add a systemd service file for vland

 -- Steve McIntyre <steve.mcintyre@linaro.org>  Thu, 24 Sep 2015 17:00:03 -0700

vland 0.3

 * Massive numbers of bugfixes and code cleanups
 * Added two new switch drivers:
   + TP-Link TL-SG2XXX family (TPLinkTLSG2XXX)
   + Netgear XSM family (NetgearXSM)
 * Added "debug" option to all the switch drivers to log all interactions
 * Added internal caching of port modes within the driver core for a large
   speed-up in normal use
 * Bug fix to handling of trunk ports in the CiscoCatalyst driver,
   improving VLAN interop with other switches
 * Huge changes to the test lab, now using 5 switches and 10 hosts
 * Big improvements to the test suite:
   + Match the new test lab layout
   + Move more of the core test code into the test-common utility library
   + Massively improved the check-networks test runner for the test hosts
   + Added parsing of the UP/DOWN results in test-common to give a simple
     PASS/FAIL result for each test
   + Added more tests
 * All logging now in UTC

 -- Steve McIntyre <steve.mcintyre@linaro.org>  Fri, 31 Jul 2015 16:21:50 +0100

vland 0.2

 * Massive numbers of bugfixes and code cleanups
 * Improve how we talk to the Cisco switches - disable paging on long
   output
 * Switch from "print" to "logging.foo" for messages, and add logfile
   support
 * Improved test suite coverage, and added core test scripts for the lab
   environment

 -- Steve McIntyre <steve.mcintyre@linaro.org>  Fri, 13 Feb 2015 15:42:31 +0800

vland 0.1

 * Initial Release

 -- Steve McIntyre <steve.mcintyre@linaro.org>  Wed, 24 Dec 2014 18:31:24 +0000

