#! /usr/bin/env python
#
# Copyright (C) 2014-2016 Linaro Limited
#
#  SPDX-License-Identifier: GPL-2.0-or-later
#
# Author: Steve McIntyre <steve.mcintyre@linaro.org>
#
# This file is part of VLANd
#
# VLANd is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# VLANd is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
# or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public
# License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, see <http://www.gnu.org/licenses>.

from setuptools import setup, find_packages

setup(
    name='vland',
    version="0.8",
    author="Steve McIntyre",
    author_email="vland-devel@linaro.org",
    license="GPL2+",
    description="VLAN daemon - complex networks on demand",
    long_description="""
 VLANd is a python program intended to make it easy to manage
 port-based VLAN setups across multiple switches in a network. It is
 designed to be vendor-agnostic, with a clean pluggable driver API to
 allow for a wide range of different switches to be controlled
 together.
    """,
    url='https://git.lavasoftware.org/lava/vland',
    packages=find_packages(),
    package_data={
        # Include the test scripts and data
        '': ['test/*'],
    },
    install_requires=[
        'psycopg2'
    ],
    data_files=[
        ("/etc/", ["vland.cfg"])
    ],
    scripts=["vland", "vland-admin"],
    zip_safe=False,
    include_package_data=True)
