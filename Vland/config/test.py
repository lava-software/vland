import unittest, os, sys
vlandpath = os.path.abspath(os.path.normpath(os.path.dirname(sys.argv[0])))
sys.path.insert(0, vlandpath)
sys.path.insert(0, "%s/../.." % vlandpath)

os.chdir(vlandpath)

from Vland.config.config import VlanConfig
from Vland.errors import ConfigError

class MyTest(unittest.TestCase):

    # Check that we raise on missing database section
    def test_missing_database_section(self):
        with self.assertRaisesRegexp(ConfigError, 'No database'):
            config = VlanConfig(filenames=("/dev/null",))
            del config

    # Check that we raise on broken database config values
    def test_missing_database_config(self):
        with self.assertRaisesRegexp(ConfigError, 'Invalid database'):
            config = VlanConfig(filenames=("test-invalid-DB.cfg",))
            del config

    # Check that we raise on missing database config values
    def test_missing_dbname(self):
        with self.assertRaisesRegexp(ConfigError, 'Database.*incomplete'):
            config = VlanConfig(filenames=("test-missing-dbname.cfg",))
            del config

    # Check that we raise on missing database config values
    def test_missing_db_username(self):
        with self.assertRaisesRegexp(ConfigError, 'Database.*incomplete'):
            config = VlanConfig(filenames=("test-missing-db-username.cfg",))
            del config

    # Check that we raise on broken vland config values
    def test_missing_vlan_config(self):
        with self.assertRaisesRegexp(ConfigError, 'Invalid vland'):
            config = VlanConfig(filenames=("test-invalid-vland.cfg",))
            del config

    # Check that we raise on broken logging level
    def test_invalid_logging_level(self):
        with self.assertRaisesRegexp(ConfigError, 'Invalid logging.*level'):
            config = VlanConfig(filenames=("test-invalid-logging-level.cfg",))
            del config

    # Check that we raise when VLANd and visn are configured for the
    # same port
    def test_clashing_ports(self):
        with self.assertRaisesRegexp(ConfigError, 'Invalid.*distinct port'):
            config = VlanConfig(filenames=("test-clashing-ports.cfg",))
            del config

    # Check that we raise on repeated switch names
    def test_missing_repeated_switch_names(self):
        with self.assertRaisesRegexp(ConfigError, 'same name'):
            config = VlanConfig(filenames=("test-reused-switch-names.cfg",))
            del config

    # Check that we raise on unknown config section
    def test_unknown_config(self):
        with self.assertRaisesRegexp(ConfigError, 'Unrecognised config'):
            config = VlanConfig(filenames=("test-unknown-section.cfg",))
            del config

    # Check we get expected values on a known-good config
    def test_known_good(self):
        config = VlanConfig(filenames=("test-known-good.cfg",))
        self.assertEqual(config.database.server, 'foo')
        self.assertEqual(config.database.port, 123)
        self.assertEqual(config.database.dbname, 'vland')
        self.assertEqual(config.database.username, 'user')
        self.assertEqual(config.database.password, 'pass')

        self.assertEqual(config.vland.port, 9997)
        self.assertEqual(config.vland.default_vlan_tag, 42)

        self.assertEqual(len(config.switches), 3)
        self.assertEqual(config.switches["10.172.2.51"].name, '10.172.2.51')
        self.assertEqual(config.switches["10.172.2.51"].driver, 'CiscoSX300')
        self.assertEqual(config.switches["10.172.2.51"].username, 'cisco_user')
        self.assertEqual(config.switches["10.172.2.51"].password, 'cisco_pass')
        self.assertEqual(config.switches["10.172.2.51"].enable_password, 'foobar')
        self.assertEqual(config.switches["10.172.2.51"].driver, 'CiscoSX300')

        self.assertEqual(config.switches["10.172.2.52"].name, '10.172.2.52')
        self.assertEqual(config.switches["10.172.2.52"].driver, 'CiscoSX300')
        self.assertEqual(config.switches["10.172.2.52"].username, 'cisco')
        self.assertEqual(config.switches["10.172.2.52"].password, 'cisco')
        self.assertEqual(config.switches["10.172.2.52"].enable_password, None)
        self.assertEqual(config.switches["10.172.2.52"].driver, 'CiscoSX300')

        self.assertEqual(config.switches["baz"].name, 'baz')
        self.assertEqual(config.switches["baz"].driver, 'CiscoSX300')
        self.assertEqual(config.switches["baz"].username, 'cisco')
        self.assertEqual(config.switches["baz"].password, 'cisco')

if __name__ == '__main__':
    unittest.main()
