#!/bin/bash
#
# VLANd test script

set -e

NAME="medium-3switches-1vlan-1"

DESCRIPTION="Check VLAN interoperability for 3 machines on 3 different switches, with an observer on a different switch left on the default VLAN"

# List all the switches and hosts we need to use, if not using all of
# them. We can make tests run faster by not involving *all* of them in
# every test.
HOSTS="panda01 arndale02 arndale03" # The machines which will be moved
				    # to the test VLAN

HOSTS="$HOSTS imx5302"              # And add another which will stay on
				    # the base VLAN as an observer

#SWITCHES=                          # We don't know which switches we're using
                                    # for the test, so let the test wrapper
                                    # work it out

# Show more detail during test output
VERBOSE=0

# And give a filename for logging
LOGFILE=$0-$$.log

# Include the core test wrapper code that makes life easier
DIR=$(dirname $0)
. ${DIR}/test-common

# Ensure all the ports we're using are on their base VLANs
log "checking base VLANs"
verify_all_hosts_are_base
log "$HOSTS are all on their base VLANs - good"

# Clear old logfiles from our test machines
stop_logging
clear_logs

# Start all the test machines logging, then wait 60s to let all of
# them show baseline results before we start testing
start_logging
log "CHECK INIT START"
log "CHECK INIT CHECK VLAN_BASE:panda01:arndale02:arndale03:imx5302"
pause 60
log "CHECK INIT END"

# Create a VLAN with tag 30, named "test30"
log "Creating new VLAN tag 30"
OUTPUT=$(run_admin_command create_vlan --name test30 --tag 30 --is_base_vlan false)
VLAN_ID=$(run_admin_command lookup_vlan_by_tag --tag 30)
log "Created new VLAN with ID $VLAN_ID"

# Wait 10s for everything to settle
pause 10

# Move some of the test machines to this new VLAN
log "Moving ports to VLAN ID $VLAN_ID"
OUTPUT=$(run_admin_command set_port_current_vlan --port_id ${panda01_PORT_ID} --vlan_id $VLAN_ID)
OUTPUT=$(run_admin_command set_port_current_vlan --port_id ${arndale02_PORT_ID} --vlan_id $VLAN_ID)
OUTPUT=$(run_admin_command set_port_current_vlan --port_id ${arndale03_PORT_ID} --vlan_id $VLAN_ID)
log "Done moving ports to VLAN ID $VLAN_ID"

# Wait 60s for everything to settle and logs to show it again
pause 60

log "CHECK STEP1 START"
log "CHECK STEP1 CHECK VLAN_30:panda01:arndale02:arndale03"
pause 60
log "CHECK STEP1 END"

# Move test machines back to their base VLANs
log "Moving ports back to base"
OUTPUT=$(run_admin_command restore_port_to_base_vlan --port_id ${panda01_PORT_ID})
OUTPUT=$(run_admin_command restore_port_to_base_vlan --port_id ${arndale02_PORT_ID})
OUTPUT=$(run_admin_command restore_port_to_base_vlan --port_id ${arndale03_PORT_ID})
log "Done moving ports back to base"

# Wait 30s for everything to settle
pause 30

log "CHECK FINI START"
log "CHECK FINI CHECK VLAN_BASE:panda01:arndale02:arndale03:imx5302"
pause 60
log "CHECK FINI END"

# Check that they're all back on their base VLANs
log "Checking base VLANs after the test"
verify_all_hosts_are_base

log "Delete the test VLAN"
OUTPUT=$(run_admin_command delete_vlan --vlan_id ${VLAN_ID})

# Stop all the test machines logging (and wait 20s for things to
# recover and get logged)
stop_logging
pause 20

# Grab logs from the machines
grab_logs >> $LOGFILE

# Clear old logs
clear_logs

# How did the test do?
check_test_steps

# DONE!
